create table pvi.students(
	id serial primary key,
    group_name varchar(64) not null,
    first_name varchar(128) not null,
    last_name varchar(128) not null,
    gender varchar(8) not null,
    birthday date not null,
    foreign key(group_name) references pvi.groups(name)
)