import http from "http";
import path from "path";
import { fileURLToPath } from 'url';

import mongoose from 'mongoose';
import express from "express";
import { Server } from "socket.io";
import dotenv from "dotenv";
import cookieParser from "cookie-parser"

import {default as studentsRouter} from "./src/routers/students.js"
import {default as chatRouter} from "./src/routers/chat.js"
import {default as authRouter} from "./src/routers/auth.js"
import {default as userRouter} from "./src/routers/user.js"

import {formatMessage} from "./src/utils/chat.js";
import {User} from "./src/schemas/user.js";
import {Message} from "./src/schemas/chat.js";


dotenv.config();

const app = express();
const server = http.createServer(app)
const io = new Server(server);
mongoose.connect(process.env.MONGO_URI)

app.use(cookieParser());
app.use(express.json())
app.set('view engine', 'ejs');

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
app.use("/static", express.static(path.join(__dirname, "/static")))
app.set('views', path.join(__dirname, "/src/views"));


let socketUsers = {}

io.on("connection", socket => {
  socket.on("joinChat", ({user_id, username, chat_id}) => {
    socketUsers[socket.id] = {
      user_id: user_id,
      username: username,
      chat_id: chat_id
    }
    socket.join(chat_id)

    socket.on("chatMessage", async (text) => {
      let user = await User.findOne({_id: new mongoose.Types.ObjectId(user_id)})
      let message = await Message.create({chat_id, user, text})
      io.to(chat_id).emit("message", formatMessage(message))
    })
  })

  socket.on("disconnect", () => {
    delete socketUsers[socket.id]
  })
})


app.use("/students", studentsRouter);
app.use("/chat", chatRouter);
app.use("/auth", authRouter);
app.use("/user", userRouter);


const PORT = 3000 || process.env.PORT;

server.listen(
  PORT,
  () => {console.log(`Server started on port ${PORT}`)}
)
