<?php
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header("Access-Control-Allow-Headers: *");

    $date_format = "d.m.Y";
    $status_to_response_code_map = array("success" => 200, "error" => 400);

    try {
        $env = parse_ini_file('../.env');
        $conn = new mysqli("localhost", "root", $env["DB_PASSWORD"], "pvi");

        function validateDate($date): bool
        {
            $year = $date->format("Y");
            if (2000 <= $year && $year <= 2005) return true;
            else return false;
        }

        function validateStudent($data): array
        {
            global $date_format;
            $keys = array("group_name", "first_name", "last_name", "gender", "birthday");

            foreach ($keys as $key) {
                if (!isset($data->$key)) {
                    return array("status" => "error", "data" => "Field " . $key . " not set");
                }
            }

            // each key validation
            $name_pattern = "/^[A-Za-zА-Яа-я]+$/";

            if (
                !preg_match($name_pattern, $data->first_name) ||
                !preg_match($name_pattern, $data->last_name)
            ) {
                return array("status" => "error", "data" => "Name fields do not match the pattern " . $name_pattern);
            } else if (!DateTime::createFromFormat($date_format, $data->birthday)) {
                return array("status" => "error", "data" => "Invalid date format");
            } else if (!validateDate(DateTime::createFromFormat($date_format, $data->birthday))) {
                return array("status" => "error", "data" => "Date is invalid");
            } else {
                return array("status" => "success", "data" => $data);
            }
        }

        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            $result = $conn->query("select * from students");
            echo json_encode($result->fetch_all(mode: MYSQLI_ASSOC));
        } else if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $body = file_get_contents('php://input');
            $object = json_decode($body);
            $validated = validateStudent($object);
            http_response_code($status_to_response_code_map[$validated["status"]]);

            if ($validated["status"] == "success") {
                $query = $conn->prepare(
                    "insert into students(group_name, first_name, last_name, gender, birthday) values \n" .
                    "(?, ?, ?, ?, ?)"
                );
                $formatted_date = DateTime::createFromFormat($date_format, $object->birthday)->format(DATE_ATOM);
                $query->bind_param(
                    "sssss",
                    $object->group_name,
                    $object->first_name,
                    $object->last_name,
                    $object->gender,
                    $formatted_date
                );
                $query->execute();
                $validated["data"]->id = $conn->insert_id;
            }

            echo json_encode($validated);
        } else if ($_SERVER["REQUEST_METHOD"] == "PUT") {
            $body = file_get_contents('php://input');
            $object = json_decode($body);

            if (!isset($object->id)) {
                http_response_code($status_to_response_code_map["error"]);
                echo json_encode(array("status" => "error", "data" => "Field id not set"));
                return;
            }

            $validated = validateStudent($object);
            http_response_code($status_to_response_code_map[$validated["status"]]);

            if ($validated["status"] == "success") {
                $query = $conn->prepare(
                    "update students \n" .
                    "set\n" .
                    "   group_name = ?,\n" .
                    "   first_name = ?,\n" .
                    "   last_name = ?,\n" .
                    "   gender = ?,\n" .
                    "   birthday = ?\n" .
                    "where id = ?"
                );
                $formatted_date = DateTime::createFromFormat($date_format, $object->birthday)->format(DATE_ATOM);
                $query->bind_param(
                    "sssssi",
                    $object->group_name,
                    $object->first_name,
                    $object->last_name,
                    $object->gender,
                    $formatted_date,
                    $object->id
                );
                $query->execute();
            }

            echo json_encode($validated);
        } else if ($_SERVER["REQUEST_METHOD"] == "DELETE") {
            $body = file_get_contents('php://input');
            $object = json_decode($body);

            if (!isset($object->id)) {
                $response = array("status" => "error", "data" => "Field id is not set");
            } else {
                $query = $conn->prepare("delete from students where id = ?");
                $query->bind_param("i", $object->id);
                $query->execute();
                $response = array("status" => "success");
            }
            http_response_code($status_to_response_code_map[$response["status"]]);
            echo json_encode($response);
        }
    } catch(Exception) {
        http_response_code(400);
        echo json_encode(array("status" => "error", "data" => "Server error"));
    }