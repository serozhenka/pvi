import { User } from "../schemas/user.js"

async function isAuthenticated(req) {
  let username = req.cookies.username;
  let result = await User.findOne({username: username});
  return result !== null;
}

async function authMiddleware(req, res, next) {
  if (await isAuthenticated(req)) return next();
  else return res.redirect("/students");
}

export {
  isAuthenticated,
  authMiddleware,
}