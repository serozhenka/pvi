import express from "express";

import { isAuthenticated} from "../middlewares/auth.js"


const router = express.Router()

router.get("/", async (req, res) => {
  return res.render("students", {
    current_page: "students",
    is_authenticated: await isAuthenticated(req),
  })
})

export default router;