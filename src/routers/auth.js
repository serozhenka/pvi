import express from "express";

import {User} from "../schemas/user.js";


const router = express.Router()

router.post("/login", async (req, res) => {
  let username = req.body.username;
  let user = await User.getOrCreateByUsername(username);
  res.status(200).send(user)
})

export default router;