import express from "express";

import {User} from "../schemas/user.js";


const router = express.Router()


router.get("/search", async (req, res) => {
  let result = []
  if (req.query.q) {
    result = await User.find({
      username: {
        "$regex": req.query.q,
        "$ne": req.cookies.username,
      }
    })
  }
  res.status(200).send(result)
})


export default router;