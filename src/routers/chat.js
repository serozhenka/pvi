import express from "express";

import {authMiddleware, isAuthenticated} from "../middlewares/auth.js"
import {User} from "../schemas/user.js";
import {Chat, Message} from "../schemas/chat.js";
import mongoose from "mongoose";
import {formatMessage} from "../utils/chat.js";


const router = express.Router()


async function getUserChats(id) {
  let chats = await Chat.find({"users._id": {$in: [new mongoose.Types.ObjectId(id)]}})
  chats.forEach(chat => {
    chat.users = chat.users.filter(user => {return !user._id.equals(new mongoose.Types.ObjectId(id))} )
  })
  return chats
}


router.get("/:id", authMiddleware, async (req, res) => {
  if (!(mongoose.Types.ObjectId.isValid(req.params.id))) {
    return res.redirect("/chat");
  }

  let chat = await Chat.findOne({
    _id: new mongoose.Types.ObjectId(req.params.id),
    "users._id": {$in: [new mongoose.Types.ObjectId(req.cookies.id)]}
  })

  if (chat == null) {
    return res.redirect("/chat");
  }

  return res.render("chat", {
    current_page: "chat",
    is_authenticated: await isAuthenticated(req),
    chats: await getUserChats(req.cookies.id),
    chat: chat,
    chat_id: req.params.id,
    messages: (await Message.find({chat_id: req.params.id}).sort({timestamp: -1})).map(msg => formatMessage(msg))
  })
})


router.get("/", authMiddleware, async (req, res) => {
  return res.render("chat", {
    current_page: "chat",
    is_authenticated: await isAuthenticated(req),
    chats: await getUserChats(req.cookies.id),
    chat_id: null,
  })
})


router.post("/", authMiddleware, async (req, res) => {
  let users = await User.find({_id: {"$in": req.body.users}}).exec()
  if (users.length !== req.body.users.length) {
    res.status(400).send({"status": "error", "description": "One or more users does not exist"})
    return;
  }
  let elemMatchLookup = req.body.users.map(user => ({"$elemMatch": {"$eq": user}}))
  let data = req.body
  data.users = users

  let chat = await Chat.findOneAndUpdate(
    {users: {"$all": users}},
    data,
    {returnOriginal: false, upsert: true}
  )

  res.status(200).send({"id": chat._id})
})

export default router;