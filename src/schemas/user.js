import mongoose from 'mongoose';
const { Schema } = mongoose;

let UserSchema = new Schema({
  username: String
});

UserSchema.statics.getOrCreateByUsername = async function (username) {
  let user = await this.model("User").findOne({username: username})
  if (!(user)) {
    user = await this.model("User").create({username: username})
  }
  return {
    id: user._id,
    username: user.username
  }
};

let User = mongoose.model('User', UserSchema);

export {
  User,
}