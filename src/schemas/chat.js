import mongoose from 'mongoose';
import {User} from "./user.js";
const { Schema } = mongoose;


let ChatSchema = new Schema({
  type: String,
  name: String,
  users: Array,
})

let MessageSchema = new Schema({
  chat_id: String,
  text: String,
  user: Object,
  timestamp: { type : Date, default: Date.now }
})

let Chat = mongoose.model('Chat', ChatSchema);
let Message = mongoose.model('Message', MessageSchema);

export {
  Chat,
  Message,
}