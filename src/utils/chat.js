import moment from "moment";

function formatMessage(message) {
  return {
    username: message.user.username,
    text: message.text,
    timestamp: moment(message.timestamp).format('DD/MM h:mm a')
  };
}

export {
  formatMessage
}