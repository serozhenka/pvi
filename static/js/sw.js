const CACHE = 'cache-first-v3';

const staticUrls = [
  "/students.ejs",
  "/style.css",
  "/js/common.js",
]

self.addEventListener('install', async (event) => {
  const cache = await caches.open(CACHE)
  await cache.addAll(staticUrls)
});

self.addEventListener('activate', async (event) => {
    const cacheNames = await caches.keys()
    await Promise.all(
      cacheNames
        .filter(name => name !== CACHE)
        .map(name => caches.delete(name))
    )
});

self.addEventListener('fetch', async (event) => {
  event.respondWith(cacheFirst(event.request))
});

async function cacheFirst(request) {
  const cached = await caches.match(request)
  return cached ?? await fetch(request)
}
