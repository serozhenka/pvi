let searchInput = document.getElementById("searchInput")
let createGroupSearchInput = document.getElementById("createGroupSearchInput")
let hitsBox = document.getElementById("hits")
let createGroupNameInput = document.getElementById("createGroupNameInput")
let groupHitsBox = document.getElementById("groupHits")
let addedUsersBox = document.getElementById("addedUsers")


searchInput?.addEventListener("input", async (e) => {
  await updateSearch(searchInput.value, hitsBox)
})

createGroupSearchInput?.addEventListener("input", async (e) => {
  await updateGroupSearch(createGroupSearchInput.value, groupHitsBox)
})


async function updateSearch(q, hitsBox) {
  hitsBox.style.display = (q === "" ? "none": "block");
  hitsBox.innerHTML = '';

  let response = await fetch(path + `/user/search?q=${q}`).then(res => res.json())
  response.forEach(obj => {
    let element = createHitElement(obj._id, obj.username, async (e) => {
      let chat = await getOrCreateChat({
        type: "private",
        users: [e.target.dataset.user_id, getCookie("id")],
      })
      window.location.href = `/chat/${chat.id}`
    })
    hitsBox.appendChild(element)
  })
}

async function updateGroupSearch(q, hitsBox) {
  let addedUsersIds = Array.from(addedUsersBox.childNodes).map(v => v.dataset.user_id)
  hitsBox.style.display = (q === "" ? "none": "block");
  hitsBox.innerHTML = '';

  let response = await fetch(path + `/user/search?q=${q}`).then(res => res.json())
  response.forEach(obj => {
    if (!(addedUsersIds.includes(obj._id))) {
      let element = createHitElement(obj._id, obj.username, async (e) => {
        addedUsersBox.appendChild(createHitElement(obj._id, obj.username, null))
        groupHitsBox.removeChild(e.target)
      })
      hitsBox.appendChild(element)
    }
  })
}

function createHitElement(id, username, eventListener) {
  let div = document.createElement("div")
  div.classList.add("d-flex", "align-items-center", "pt-3", "pb-3", "chat-search-hit")
  div.dataset.user_id = id
  div.innerHTML = `
      <img class="d-block ms-2 me-3 rounded-circle" width="45" src="/static/images/default-user-image.png" style="pointer-events: none"/>
      <span style="pointer-events: none">${username}</span>
    `
  div.addEventListener("click", eventListener)
  return div
}

async function getOrCreateChat(data) {
    let response = await fetch(path + `/chat`, {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(data)
    })
    return await response.json();
}

document.getElementById("createGroupInit")?.addEventListener(
  "click",
  (e) => {
    groupHitsBox.innerHTML = '';
    createGroupSearchInput.value = ""
    groupHitsBox.style.display = "none"
    $("#createGroupModal").modal("show")
  }
)

document.getElementById("createGroupSubmit").addEventListener(
  "click",
  async (e) => {
    if (!(createGroupNameInput.value && Array.from(groupHitsBox.childNodes).length)) {
      return
    }

    let usersIds = Array.from(addedUsersBox.childNodes).map(v => v.dataset.user_id)
    let chat = await getOrCreateChat({
      name: createGroupNameInput.value,
      type: "group",
      users: [getCookie("id")].concat(usersIds),
    })

    window.location.href = `/chat/${chat.id}`
  }
)

function createMessageElement({username, text, timestamp}) {
  let fromMe = getCookie("username") === username
  let div = document.createElement('div')
  div.classList.add('d-flex', 'mt-3')

  if (fromMe) div.classList.add("justify-content-end")


  div.innerHTML = `
    <div class="py-2 px-3 rounded ${fromMe ? "bg-light-blue" : "bg-light-gray"}" style="max-width: 70%">
        <div class="text-muted" style="font-size: 12px">${fromMe ? "you" : username}</div>
        <div class="message-box" style="">${text}</div>
        <div class="d-flex justify-content-end align-items-center" style="font-size: 12px">
            <div class="text-end text-muted mt-1">${timestamp}</div>
        </div>
    </div>
  `

  return div
}

