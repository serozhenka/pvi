let path = `${location.protocol}//${location.host}`

document.getElementById("checkbox-checkAll")?.addEventListener(
  "click",
  (e) => {
      for (let i = 0; i < studentTableBody.childNodes.length; i++) {
          let input = studentTableBody.childNodes[i].getElementsByClassName("form-check-input")[0]
          if (
              (e.currentTarget.checked && !input.checked) ||
              (!e.currentTarget.checked && input.checked)
          ) input.click()
      }
    }
)

document.getElementById("notificationBell").addEventListener(
    "dblclick",
    (e) => {
        document.getElementById("notificationBellCircle").style.backgroundColor = "#f53f3f"
    }
)

window.addEventListener("load", () => {updateUserUsername()})

function updateUserUsername() {
  document.getElementById("username").innerHTML = getCookie("username") || ""
}

document.getElementById("authButton")?.addEventListener(
  "click",
  () => {
    $("#authModal").modal("show")
  }
)

document.getElementById("loginButton")?.addEventListener(
  "click",
  async () => {
    let username = document.getElementById("usernameInput").value
    if (username) {
      fetch(path + "/auth/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({"username": username})
      })
        .then(async (res) => {
          if (res.ok) {
            let data = await res.json()
            setCookie("id", data.id, 14)
            setCookie("username", data.username, 14)
            updateUserUsername()
            location.reload()
          }
        })
    }
  }
)

document.getElementById("logoutButton")?.addEventListener(
  "click",
  () => {
    eraseCookie("username")
    eraseCookie("id")
    updateUserUsername()
    location.reload()
  }
)
