const zeroPad = (num, places) => String(num).padStart(places, '0')

function formatDate(date) {
  return zeroPad(date.getDate(), 2) + "." + zeroPad(date.getUTCMonth() + 1, 2) + "." + date.getUTCFullYear()
}

function dateFromString(string) {
  let data = string.split(".")
  return new Date(Number(data[2]), Number(data[1]) - 1, Number(data[0]) + 1)
}