let apiPath = `${location.protocol}//${location.hostname}:8000/students.php`
let studentTableBodyId = "students-table-tbody"
let studentTableBody = document.getElementById(studentTableBodyId)
let studentModalHeader = document.getElementById("addEditStudentModal-header")
let studentModalButton = document.getElementById("addEditStudentForm-button")
let studentModal = $("#addEditStudentModal")
let studentForm = document.getElementById("addEditStudentForm")

window.onload = async () => {
  let students = await apiGetStudents();
  students.forEach(student => createStudent(Student.fromJson(student)))
}


class Student {
  constructor(id, group_name, first_name, last_name, gender, birthday) {
    this.id = id;
    this.group_name = group_name;
    this.first_name = first_name;
    this.last_name = last_name;
    this.gender = gender;
    this.birthday = birthday;
  }

  static fromJson(data) {
    return new Student(data.id, data.group_name, data.first_name, data.last_name, data.gender, data.birthday)
  }

  toObject() {
    return {
      id: this.id,
      group_name: this.group_name,
      first_name: this.first_name,
      last_name: this.last_name,
      gender: this.gender,
      birthday: this.birthday,
    }
  }
}

function openAddStudentModal() {
  studentForm.dataset.current_action = "add"
  document.getElementById("addEditStudentForm").reset()
  studentModalHeader.innerHTML = "Add student"
  studentModalButton.innerHTML = "Create"
  studentModal.modal("show")
  document.getElementById("addEditStudentForm-idInput").value = studentTableBody.childNodes.length
}

function openEditStudentModal(element) {
  studentForm.dataset.current_action = "edit"
  studentModalHeader.innerHTML = "Edit student"
  studentModalButton.innerHTML = "Edit"
  studentModal.modal("show")

  let tr = element.parentNode.parentNode
  const getTrDataByClassName = (className) => tr.getElementsByClassName(className)[0].innerText

  document.getElementById("addEditStudentForm-idInput").value = tr.id
  document.getElementById("addEditStudentForm-groupInput").value = getTrDataByClassName("tr-student-group")
  document.getElementById("addEditStudentForm-FirstNameInput").value = getTrDataByClassName("tr-student-name").split(" ")[0]
  document.getElementById("addEditStudentForm-LastNameInput").value = getTrDataByClassName("tr-student-name").split(" ")[1]
  document.getElementById("addEditStudentForm-genderInput").value = getTrDataByClassName("tr-student-gender")
  document.getElementById("addEditStudentForm-birthdayInput").valueAsDate = dateFromString(getTrDataByClassName("tr-student-birthday"))
}

function selectTableRow(tr) {
  if (tr.find("[type=checkbox]").is(":checked")) tr.addClass("highlight-row")
  else tr.removeClass("highlight-row")
}

function deleteStudents() {
  let checkboxes = Array.from(document.getElementsByClassName("student-checkbox"))
  checkboxes.forEach(checkbox => {
    if (checkbox.checked) {
      checkbox.parentNode.parentNode.remove()
      apiDeleteStudent(checkbox.parentNode.parentNode.id);
    }
  })
}

function showToast(message, bg_color = "#ffffff") {
  document.getElementById("toast-header").style.setProperty("background-color", bg_color, "important")
  document.getElementById("toast-body").innerText = message
  $("#toast").toast('show')
}

function selectClosestCheckbox(element) {
  element.closest("tr").find("[type=checkbox]").prop("checked", true)
}

function createStudent(student, status= "offline", dateFormatted = false) {
  let studentElement = document.createElement("tr")
  studentElement.id = student.id
  studentElement.innerHTML = `
      <td><input class="form-check-input mt-0 disable-focus student-checkbox" type="checkbox" value="" aria-label="" onclick="selectTableRow($(this.parentNode.parentNode))"></td>
      <td class="tr-student-group">${student.group_name}</td>
      <td class="tr-student-name">${student.first_name + ' ' + student.last_name}</td>
      <td class="tr-student-gender">${student.gender}</td>
      <td class="tr-student-birthday">${dateFormatted ? student.birthday: formatDate(new Date(student.birthday))}</td>
      <td class="tr-student-status">${status}</td>
      <td>
        <i class="me-1 fa-solid fa-pen hover-link" onclick="openEditStudentModal(this)"></i>
        <i class="me-1 fa-solid fa-xmark hover-link" data-bs-toggle="modal" onclick="selectClosestCheckbox($(this.parentNode)); selectTableRow($(this.parentNode.parentNode))" data-bs-target="#deleteStudentModal"></i>
      </td>        
  `
  studentTableBody.appendChild(studentElement)
}

function editStudent(tr, student) {
  const getTrDataByClassName = (className) => tr.getElementsByClassName(className)[0]

  getTrDataByClassName("tr-student-group").innerText = student.group_name
  getTrDataByClassName("tr-student-name").innerText = student.first_name + " " + student.last_name
  getTrDataByClassName("tr-student-gender").innerText = student.gender
  getTrDataByClassName("tr-student-birthday").innerText = student.birthday
}

document.getElementById("addEditStudentForm").addEventListener(
  "submit",
  async (e) => {
    e.preventDefault()
    let form = e.target
    let current_action = form.dataset.current_action;

    let data = {
      "group_name": form.group.value.toUpperCase(),
      "first_name": form.first_name.value,
      "last_name": form.last_name.value,
      "gender": form.gender.value,
      "birthday": formatDate(new Date(form.birthday.value)),
    }

    let response;

    if (current_action === "add") {
      response = await apiCreateStudent(Student.fromJson(data))
    } else if (current_action === "edit") {
      data["id"] = form.id.value
      response = await apiEditStudent(Student.fromJson(data))
    }

    if (response) {
      showToast(`Successfully ${current_action}ed a student`, "#a4d0a4")
      if (current_action === "add") {
        createStudent(Student.fromJson(response), "offline", true)
        form.reset()
      } else {
        let tr = Array.from(studentTableBody.childNodes).filter(v => v.id === data.id)[0]
        editStudent(tr, Student.fromJson(data))
      }
    }

    studentModal.modal('hide')
  }
)

async function apiCreateStudent(student) {
  let response = await fetch(apiPath, {
    headers: {"Accept": "application/json", "Content-Type": "application/json"},
    method: "POST",
    body: JSON.stringify(student.toObject())
  })

  let response_data = await response.json()

  if (response.ok) { return response_data["data"] }
  else { showToast(response_data["data"], "#d56f7a"); return null;}
}

async function apiEditStudent(student) {
  let response = await fetch(apiPath, {
    headers: {"Accept": "application/json", "Content-Type": "application/json"},
    method: "PUT",
    body: JSON.stringify(student.toObject())
  })

  let response_data = await response.json()

  if (response.ok) { return response_data["data"] }
  else { showToast(response_data["data"], "#d56f7a"); return null;}
}

async function apiGetStudents() {
  let response = await fetch(apiPath)
  let data = await response.json()

  if (response.ok) { return data }
  else { showToast(data["data"], "#d56f7a"); return null;}
}

async function apiDeleteStudent(id) {
  let response = await fetch(
    apiPath,
    {
      method: "DELETE",
      body: JSON.stringify({"id": id}),
    }
  )
  let data = await response.json()

  if (response.ok) { return data }
  else { showToast(response["data"], "#d56f7a"); return null;}
}


